﻿import QtQuick 2.0

Rectangle {
    id: rect
    width: 100
    height: 20
    border.width: 1
    border.color: "gray"

    property var currentDate: new Date

    Component.onCompleted: {
        label.text = getFormat(currentDate)
    }

    function up(){
        var time = currentDate.getTime() + 1*24*60*60*1000
        currentDate.setTime(time)
        label.text = getFormat(currentDate)
    }

    function down(){
        var time = currentDate.getTime() - 1*24*60*60*1000
        currentDate.setTime(time)
        label.text = getFormat(currentDate)
    }

    function addZero(value){
        if(value<10){
            return '0' + value
        }
        else{
            return value
        }
    }

    function getFormat(date){
        var year = date.getFullYear()
        var month = date.getMonth()
        var day = date.getDate()

        return year + '/' + addZero(month+1) + '/' + addZero(day)
    }

    TextInput{
        id: label
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width-imgUp.width
        readOnly: true
        selectByMouse: true

    }
    Image {
        id: imgUp
        source: "qrc:/up.png"
        anchors.right: parent.right
        anchors.rightMargin: 2
        y: 2

        MouseArea{
            anchors.fill: parent
            onClicked: up()
        }
    }
    Image {
        id: imgDown
        source: "qrc:/down.png"
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 2
        anchors.rightMargin: 2

        MouseArea{
            anchors.fill: parent
            onClicked: down()
        }
    }
}
